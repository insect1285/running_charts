# About
A data visualization of data downloaded as an XML from RunningAhead.com.  Data are visualized in 2-D Column Chart and
3-D Scatter plot using the Plotly.js library.

This application provides methods of aggregation and viewing the data to demonstrate some options available when the
data are manipulated based on user inputs and to show the capabilities of the Plotly library.

# Contributors
Author:  R. Dylan Walker (dylan_walker at yahoo dot com)


