/**
 * Class to scaffold the traces used in the plotly plots *
 */
class Trace {
    /**
     * Trace constructor method
     * @param name The name of the trace; used in legend and for selection
     */
    constructor(name) {
        this.name = name;
        this.times = [];
        this.distances = [];
        this.duration = [];
        this.type = "bar";
    }
}

/**
 * Builds the data into a consistent format used by Plotly
 * @param xml RunningAhead.com XML data loaded from file.
 * @returns {*[]}
 */
const build_data = (xml) => {
    // Data traces for plotly bar chart
    let trace_other = new Trace("Other Run");
    let trace_easy = new Trace("Easy Run");
    let trace_long = new Trace("Long Run");
    let trace_hill = new Trace("Hills");
    let trace_race = new Trace("Race!");
    let trace_tempo = new Trace("Tempo Run");
    let trace_interval = new Trace("Intervals");

    // Populate chart data with individual traces for Plotly
    let chart_data = [trace_other, trace_easy, trace_long, trace_hill, trace_tempo, trace_interval, trace_race];

    // Start data transform
    let data = xml.documentElement.getElementsByTagName("Event");

    /**
     * Pushes standardized data from the XML file to the class
     * @param event A single running event
     * @param trace The trace to push the data to; determined by the event subtype.
     */
    const push_data = (event, trace) => {
        trace.times.push(event.getAttribute("time"));
        trace.distances.push(event.getElementsByTagName("Distance")[0].innerHTML);
        trace.duration.push(event.getElementsByTagName("Duration")[0].getAttribute("seconds"));
    };

    // Build data for Plotly based on the event subtype (i.e., "Easy", "Hills", etc.)
    for (let i = 0; i < data.length; i++) {
        let event = data[i];
        let type = event.getAttribute("typeName");
        if (type === "Run") {
            switch (event.getAttribute("subtypeName")) {
                case "Easy":
                    push_data(event, trace_easy);
                    break;
                case "Long":
                    push_data(event, trace_long);
                    break;
                case "Hill":
                    push_data(event, trace_hill);
                    break;
                case "Tempo":
                    push_data(event, trace_tempo);
                    break;
                case "Interval":
                    push_data(event, trace_interval);
                    break;
                case "Race":
                    push_data(event, trace_race);
                    break;
                default:
                    push_data(event, trace_other);
            }
        }
    }
    return chart_data;
};

const aggregate_data = (data, group_by) => {
    // Aggregate data by user-chosen option
    data.forEach(trace => {
        trace.y = [];
        // Convert based on grouping
        trace.x = trace.times.map(time => new Date(moment(time).startOf(group_by)));
        // Remove the duplicates
        trace.x = trace.x.reduce((result_object, current_value, current_index) => {
            // check if current_value already in hash table, if not, add it to result
            //   and push to hash table
            !result_object.hash[current_value] && result_object.result.push(current_value);
            result_object.hash[current_value] = true;
            // Add corresponding distances at index together when duplicate dates are removed
            trace.y[result_object.result.length - 1] === undefined ?
                trace.y.push(parseFloat(trace.distances[current_index])) :
                trace.y[result_object.result.length - 1] += parseFloat(trace.distances[current_index]);
            return result_object;
        }, {result: [], hash: {}}).result;
    });
};

/**
 * Builds the bar chart based on the group by aggregation chosen by the user; defaults to "Week"
 * @param group_by The user-chosen group by value chosen
 * @param data The data used for the chart; Array of traces
 */
const create_bar_chart = (group_by, data) => {

    let selectorOptions = {
        buttons: [{
            step: 'month',
            stepmode: 'backward',
            count: 1,
            label: '1m'
        }, {
            step: 'month',
            stepmode: 'backward',
            count: 6,
            label: '6m'
        }, {
            step: 'year',
            stepmode: 'todate',
            count: 1,
            label: 'YTD'
        }, {
            step: 'year',
            stepmode: 'backward',
            count: 1,
            label: '1y'
        }, {
            step: 'all',
        }],
    };

    // Layout configuration
    let barchart_layout = {
        title: "Running Distance over Time",
        xaxis: {
            title: "Date",
            type: "date",
            rangeselector: selectorOptions,
            rangeslider: {}
        },
        yaxis: {
            title: "Distance (Miles)"
        },
        barmode: "stack",
        showlegend: true
    };

    aggregate_data(data, group_by);

    // Adjust axis labels for cleaner look
    if (group_by === "Year") {
        barchart_layout.xaxis.tickvals = data[0].x;
    } else {
        barchart_layout.xaxis.tickvals = null;
    }
    // Plot data to DOM
    Plotly.react('bar_chart', data, barchart_layout);
};

/**
 * Creates a 3D scatter plot of the data passed in.  Does some processing to get the data into correct format.
 * @param data Data to be used for 3D Charts; Array of traces
 */
const create_3d_chart = (data) => {

    /**
     * Calculates the pace in seconds of the event
     * @param duration_seconds Duration of the event in seconds
     * @param distance_miles Distance of the event in miles
     * @returns {number}
     */
    const calc_pace = (duration_seconds, distance_miles) => {
        return duration_seconds / distance_miles;
    };

    /**
     * Assigns size of the dot in Scatter3d plot based on average pace
     * @param pace Average Pace of the run in seconds
     * @returns {number}
     */
    const assign_size = (pace) => {
        let s = null;
        if (pace < 360) {//6-minute pace
            s = 8
        } else if (pace < 420) {//7-minute pace
            s = 7
        } else if (pace < 480) {//8-minute pace
            s = 5
        } else if (pace < 540) {//9-minute pace
            s = 4
        } else {
            s = 3 // anything slower than 9-minute pace
        }
        return s * 2;
    };


    /**
     * Creates annotations for a custom legend to show paces by size of the marker.
     * @param paces Paces of the events in minutes; Used as labels in legend text.
     * @returns {Array}
     */
    const make_annotations = (paces) => {
        let annotations = [];
        paces.forEach((pace, i) => {
            annotations.push({
                text: `${pace}-minute pace`,
                xref: 'paper',
                yref: 'paper',
                xshift: 10,
                ax: 0,
                ay: 0,
                showarrow: false,
                xanchor: 'left',
                yanchor: 'middle',
                x: 1,
                y: i / 20
            });
        });
        return annotations;
    };

    /**
     * Creates shapes for the custom legend based on pace of the run in minutes.
     * @param paces Pace of the events in minutes
     * @returns {Array}
     */
    const make_shapes = (paces) => {
        let shapes = [];
        paces.forEach((pace, i) => {
            let size_start = 0.006;
            let size_change = (i / 1000) * 2;
            let y_adjust = 0.001;
            shapes.push({
                xref: 'paper',
                yref: 'paper',
                type: "circle",
                line: {
                    width: 0
                },
                fillcolor: "rgba(100,100,100,0.75)",
                x0: 1 - size_start - size_change,
                x1: 1 + size_start + size_change,
                y0: -size_start - y_adjust + i / 20 - size_change,
                y1: size_start + y_adjust + i / 20 + size_change,
            });
        });
        return shapes;
    };

    // Days of the week; If changed then also need to change tickvals array in layout_3d.scene.yaxis.tickvals
    const days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    // Paces used for custom legend; Should match the values used in assign_size()
    const paces = ["≥9", 8, 7, 6];

    // 3D Chart
    let layout_3d = {
        title: "Running Distance By Week and Day of the Week",
        scene: {
            yaxis: {
                title: "Day",
                tickvals: [0, 1, 2, 3, 4, 5, 6],
                ticktext: days
            },
            xaxis: {
                title: "Week"
            },
            zaxis: {
                title: "Miles"
            },
            // annotations: [
            //     // Call out Annotations
            //     {
            //         showarrow: true,
            //         x: 48,
            //         y: 6,
            //         z: 13.19,
            //         text: "Rehoboth Half (PR)",
            //         font: {
            //             color: "black",
            //             size: 12
            //         },
            //         xanchor: "left",
            //         ax: 25,
            //         opacity: 0.7,
            //         arrowcolor: "black",
            //         arrowsize: 1,
            //         arrowwidth: 1,
            //         arrowhead: 1
            //     },
            //     {
            //         showarrow: true,
            //         x: 28,
            //         y: 3,
            //         z: 1.03,
            //         text: "Fastest Mile",
            //         font: {
            //             color: "black",
            //             size: 12
            //         },
            //         xanchor: "left",
            //         ax: 25,
            //         opacity: 0.7,
            //         arrowcolor: "black",
            //         arrowsize: 1,
            //         arrowwidth: 1,
            //         arrowhead: 1
            //     },
            // ]
        },
        // Pace Annotations and Shapes (functions as second legend)
        annotations: make_annotations(paces),
        shapes: make_shapes(paces),
        showlegend: true
    };

    // Assign coordinates based on the day of the week or week of the year and the number of miles
    data.forEach(trace => {
        trace.z = trace.distances;
        trace.y = trace.times.map(time => moment(time).day());
        trace.x = trace.times.map(time => moment(time).isoWeek());
        trace.type = "scatter3d";
        trace.hoverinfo = 'text';
        trace.hovertext = trace.z.map((distance_miles, i) =>
            `Date: ${moment(trace.times[i]).format("MMM D, YYYY")}` +
            `<br>Distance: ${distance_miles}<br>Week: ${trace.x[i]}` +
            `<br>Day of Week: ${days[trace.y[i]]}` +
            `<br>Avg Pace: ${moment.utc(trace.duration[i] * 1000 / distance_miles).format("mm:ss")}`);
        trace.mode = "markers";

        // Assign point size based on the pace of the run
        let size = trace.duration.map((duration_seconds, i) => {
            return assign_size(calc_pace(duration_seconds, trace.distances[i]));
        });
        trace.marker = {size: size};
    });

    Plotly.react('3d_chart', data, layout_3d);

    // Redraw if bar chart range changes
    document.getElementById('bar_chart').on('plotly_relayout', (evt) => {

        // Get min/max dates so we can filter our data
        if (document.getElementById("sync_dates").checked === true) {
            let min_date, max_date;
            // Handle zoom on both chart and the range slider
            if (evt['xaxis.autorange']) {
                min_date = moment('20170101', 'YYYYMMDD');
                max_date = moment();
            } else {
                try {
                    min_date = moment(evt['xaxis.range'][0]);
                }
                catch (err) {
                    min_date = moment(evt['xaxis.range[0]']);
                }
                try {
                    max_date = moment(evt['xaxis.range'][1]);
                }
                catch (err) {
                    max_date = moment(evt['xaxis.range[1]']);
                }
            }

            // Assign coordinates based on the day of the week or week of the year and the number of miles
            //  and filter to only the dates in the range slider if chosen by the user
            data.forEach(trace => {
                let filtered_times = trace.times.filter(time => {
                    return moment(time).isSameOrBefore(max_date, 'day') && moment(time).isSameOrAfter(min_date, 'day');
                });

                let slice_start = trace.times.indexOf(filtered_times[0]);
                let slice_end = trace.times.lastIndexOf(filtered_times[filtered_times.length-1])+1;
                let filtered_duration = trace.duration.slice(slice_start, slice_end);

                trace.z = trace.distances.slice(slice_start, slice_end);
                trace.y = trace.times.map(time => moment(time).day()).slice(slice_start, slice_end);
                trace.x = trace.times.map(time => moment(time).isoWeek()).slice(slice_start, slice_end);

                trace.type = "scatter3d";
                trace.hoverinfo = 'text';
                trace.hovertext = trace.z.map((distance_miles, i) =>
                    `Date: ${moment(filtered_times[i]).format("MMM D, YYYY")}` +
                    `<br>Distance: ${distance_miles}<br>Week: ${trace.x[i]}` +
                    `<br>Day of Week: ${days[trace.y[i]]}` +
                    `<br>Avg Pace: ${moment.utc(filtered_duration[i] * 1000 / distance_miles).format("mm:ss")}`);
                trace.mode = "markers";

                // Assign point size based on the pace of the run
                let size = filtered_duration.map((duration_seconds, i) => {
                    return assign_size(calc_pace(duration_seconds, trace.z[i]));
                });
                trace.marker = {size: size};
            });
            Plotly.redraw('3d_chart');
        }
    });
};

/**
 * Initialization function that loads the data and begins processing it.
 */
new Promise(resolve => {
    Plotly.d3.xml("./data/log.xml", "application/xml", xml => {

        // Process the data
        const data = build_data(xml);

        //Deep Copy data to avoid overwriting during manipulations
        let bar_chart_data = data.map(trace => ({...trace}));
        let scatter3d_chart_data = data.map(trace => ({...trace}));

        /**
         * Updates the Bar Chart based on the group_value chosen by the user.
         */
        const update_chart = () => {
            let group_by = groupBy_selector.value;
            aggregate_data(bar_chart_data, group_by);

            Plotly.redraw("bar_chart");
        };

        // Code to handle user change of selection; watches for a change to call functions
        let groupBy_selector = document.getElementById("group_by_select");
        groupBy_selector.addEventListener('change', update_chart, false);

        create_bar_chart("Week", bar_chart_data);
        create_3d_chart(scatter3d_chart_data);

        resolve()
    })
}).then(() => {
    document.body.removeAttribute("class");
});

